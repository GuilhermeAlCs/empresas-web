import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainModule } from './main/main.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './main/login/login.component';
import { HomeComponent } from './main/home/home.component';

const appRoutes: Routes = [
{ path: '', component: LoginComponent},
{ path: 'enterprises', component: HomeComponent},
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    MainModule,
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
