import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

import { usuario } from '../shared/usuario.model';

import { LoginService } from '../login.service';
import { credential } from '../shared/credential.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private meuUsuario: usuario;
  public userCredentials: credential;
  public isTryingToLogin: boolean = false;
  public loginFail: boolean = false;

  constructor(private servico: LoginService, private router: Router) {
  	this.servico.userCredentials.subscribe(valor => this.userCredentials = valor);
  }

  ngOnInit() {
  }

  public login(f: any ){
    this.isTryingToLogin = true;
    this.loginFail = false;
    let x = new usuario();
    x = f.value;
    if(x.password && x.email){
      console.log("submitted");
      this.servico.fazerLogin(x.email,x.password);
      setTimeout(() =>{
      this.checkLogIn(2) },5000);
    }
    else{
    	console.log("invalid from")
      this.isTryingToLogin = true;
    }
  }

  private checkLogIn(tentativas: number){
      if(this.servico.isloggedin()){
        console.log("Valido");
        this.router.navigate(['enterprises']);
      }
      else{
        if(tentativas>0){
          setTimeout(() =>{
            this.checkLogIn(tentativas-1) },2500);
          
        }
        else{
          this.isTryingToLogin = false;
          this.loginFail = true;
          console.log("Fim tentativas de login");
        }    
      }

  }
}
