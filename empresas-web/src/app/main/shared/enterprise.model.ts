import { enterpriseType } from './enterpriseType.model'
export class enterprise{
	public id: number;
	public email_enterprise: string;
	public facebook: string;
	public twitter: string;
	public linkedin: string;
	public phone: string;
	public own_enterprise: boolean;
	public enterprise_name: string;
	public photo: string;
	public description: string;
	public city: string;
	public country: string;
	public value: number;
	public share_price: number;
	public enterprise_type: enterpriseType;
}