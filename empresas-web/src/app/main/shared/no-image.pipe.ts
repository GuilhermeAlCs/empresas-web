import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noImage'
})
export class NoImagePipe implements PipeTransform {

  public base64NoImage: string = "src/app/shared/img/semfoto.png";

  transform(value: string): string {

  		if(value == ""){
  			return this.base64NoImage;
  		}
  		else{
  			return value;
  		}
  }

}
