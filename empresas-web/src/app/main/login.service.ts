import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

//rxjs
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/toPromise';
import { BehaviorSubject  } from 'rxjs/BehaviorSubject';
//models
import { credential } from './shared/credential.model';

@Injectable()
export class LoginService {

  private apiURL:any	='http://54.94.179.135:8090/api/v1/users/auth/sign_in';
  private currentUserCredentials: credential;

  //BehaviourSubject para o compartilhamento da variavel entre componentes irmaos.
  private fonteUserCredentials = new BehaviorSubject<credential>({
    uid: null,
    client: null,
    accessToken: null
  });
  userCredentials = this.fonteUserCredentials.asObservable();

  constructor(private http: Http) { 
    this.userCredentials.subscribe(valor => this.currentUserCredentials = valor);
  }

  public atribuirCredenciais(novaCredencial:credential ){
      this.fonteUserCredentials.next(novaCredencial);
  }

  public isloggedin(): boolean{
    console.log(this.currentUserCredentials);
    if(this.currentUserCredentials.uid){
      return true;
    }else{
      return false;
    }
  }

  public fazerLogin(usuario:any, senha:any){
    //Definicao do cabecalho
    const cabecalho:any = new Headers({
        'Content-Type':  'application/json'
    });
    const options = new RequestOptions({headers: cabecalho});
    //Post
  	this.http.post(
  	this.apiURL,
  	JSON.stringify({email: usuario, password: senha}),
    options)
  		.toPromise()
      .then((res:Response) => {
        let newuserCredencials = {
            uid: res.headers.get('uid'),
            client: res.headers.get('client'),
            accessToken: res.headers.get('access-token')
        };
        this.atribuirCredenciais(newuserCredencials);
	  	})
      .catch((e:any) => {
        console.log(e)
      });
  }
  
}
