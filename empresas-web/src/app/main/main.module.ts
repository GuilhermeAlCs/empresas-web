import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

import { LoginService } from './login.service';
import { EnterpriseService } from './home/enterprise.service';
import { NoImagePipe } from './shared/no-image.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
  ],
  declarations: [LoginComponent, HomeComponent, NoImagePipe],
  exports:[LoginComponent,HomeComponent],
  providers:[LoginService,EnterpriseService]
})
export class MainModule { }
