import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";


//services
import { LoginService } from '../login.service';
import { EnterpriseService } from './enterprise.service';

//models
import { credential } from '../shared/credential.model';
import { enterprise } from '../shared/enterprise.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public userCredentials: credential;
  public searchContent: string = '';
  public listaEmpresas: Array<enterprise>;

  public empresaDetalhes: enterprise;
  public isDetailScreen: boolean = false;
  public isLoggedIn = false;
  public hasPerfomedSearch = false;

  constructor(private servicoLogin: LoginService,         private servicoEnterprise: EnterpriseService, 
  private router: Router
  ) {
  	this.listaEmpresas = new Array<enterprise>();
  }

  ngOnInit() {
  	this.servicoLogin.userCredentials.subscribe(credencial => this.userCredentials = credencial);
    console.log(this.listaEmpresas);
    //Verificar se o usuário pussui UID
    this.isLoggedIn = this.servicoLogin.isloggedin();
  }

  public buscar(){
    this.hasPerfomedSearch = true;
    if(this.servicoLogin.isloggedin()){
    	//Get de todas as empresas
      this.servicoEnterprise.getEnterprisesByName(this.userCredentials, this.searchContent)
        .then((res: enterprise[]) =>{
          console.log(res)
          this.listaEmpresas = res;
          console.log(this.listaEmpresas);
        })
    }
    else{
    this.isLoggedIn = false;
    }
  }

  public mostrarDetalhes(idDetalhe: number){
      for (var index in this.listaEmpresas) {

        if(this.listaEmpresas[index].id == idDetalhe){
            this.empresaDetalhes = this.listaEmpresas[index];
            this.isDetailScreen= true;
          }
      }       
  }

  public esconderDetalhes(){
      this.isDetailScreen = false;
  }

  public retornarParaPaginaLogin(){
    this.router.navigate(['']);
  }
}
