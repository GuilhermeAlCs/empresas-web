import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

//models
import { credential } from '../shared/credential.model';
import { enterprise } from '../shared/enterprise.model';

@Injectable()
export class EnterpriseService {

  private apiURL:any ='http://54.94.179.135:8090/api/v1/enterprises';

  constructor(private http: Http) { }
 
  public getEnterprises(usuario:credential):Promise<enterprise[]>{
    //Definicao do cabecalho
    const cabecalho:any = new Headers({
        'Content-Type': 'application/json',
        'access-token': usuario.accessToken,
        'client': 		usuario.client,
        'uid': 			usuario.uid,
    });
    const options = new RequestOptions({headers: cabecalho});
    //Get
  	return this.http.get(this.apiURL,options).toPromise()
      .then(
        (res:Response) => {
          console.log(res);
        	return res.json()["enterprises"];
        })
      .catch(
        (e:any) => {
        console.log(e)
      });
  }

  public getEnterprisesByName(usuario:credential, nomeEmpresa: string):Promise<enterprise[]>{
    //Definicao do cabecalho
    const cabecalho:any = new Headers({
        'Content-Type': 'application/json',
        'access-token': usuario.accessToken,
        'client':     usuario.client,
        'uid':      usuario.uid,
    });
    const options = new RequestOptions({headers: cabecalho});
    //Get
    return this.http.get(`${this.apiURL}?name=${nomeEmpresa}`,options).toPromise()
      .then(
        (res:Response) => {
          console.log(res);
          return res.json()["enterprises"];
        })
      .catch(
        (e:any) => {
        console.log(e)
      });
  }

}
